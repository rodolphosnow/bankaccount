class AccountServices

    def self.get_account(account_id)
        account = Account.where(id: account_id)&.first
        return account
    end

    def self.get_account_balance(account_id)
        account = get_account(account_id)
        if account.present?
            return account.balance.to_f
        else
            return nil
        end
    end

end