class EventsServices

    def self.get_account(account_id)
        return AccountServices.get_account(account_id)
    end

    def self.create_account(account_id)
        account = Account.create(id: account_id, balance: 0)
        return account
    end

    def self.process_requested_event(event_data)
        case event_data[:type]
        when "deposit"
           return deposit(event_data)
        when "withdraw"
            withdraw(event_data)
        when "transfer"
            transfer(event_data)
        else
            return nil
        end
    end

    #Deposit related code
    def self.deposit(deposit_data)
        can_deposit = validate_deposit_data(deposit_data)
        return nil unless can_deposit
        account = get_account(deposit_data[:destination])
        unless account.present?
            account = create_account(deposit_data[:destination])
        end
        account.destination_events.build(type: "Deposit", amount: deposit_data[:amount])
        account.balance += deposit_data[:amount]
        account.save
        return { destination: {id: account.id.to_s, balance: account.balance.to_f} }
    end

    def self.validate_deposit_data(deposit_data)
        deposit = Event.new(type: "Deposit", amount: deposit_data[:amount], destination: deposit_data[:destination])
        return deposit.valid?
    end

    #Withdraw related code
    def self.withdraw(withdraw_data)
        return nil unless withdraw_data[:origin].present?
        account = get_account(withdraw_data[:origin])
        return nil unless account.present?
        account.origin_events.build(type: "Withdraw", amount: withdraw_data[:amount])
        account.balance -= withdraw_data[:amount]
        account.save
        return { origin: {id: account.id.to_s, balance: account.balance.to_f} }
    end

    #Transfer related code
    def self.transfer(transfer_data)
        return nil unless transfer_data[:origin].present?
        return nil unless transfer_data[:destination].present?
        origin_account = get_account(transfer_data[:origin])
        destination_account = get_account(transfer_data[:destination])
        unless destination_account.present?
            destination_account = create_account(transfer_data[:destination])
        end
        return nil unless origin_account.present? && destination_account.present?
        withdraw(transfer_data)
        deposit(transfer_data)
        origin_account.reload
        destination_account.reload
        return { origin: { id: origin_account.id.to_s, balance: origin_account.balance.to_f}, destination: {id: destination_account.id.to_s, balance: destination_account.balance.to_f } }
    end

end