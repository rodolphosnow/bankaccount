class Account < ApplicationRecord
    validates_presence_of :balance
    has_many :origin_events, class_name: "Event", foreign_key: "origin"
    has_many :destination_events, class_name: "Event", foreign_key: "destination"
end
