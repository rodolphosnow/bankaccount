class Transfer < Event
    validates_presence_of :destination, :amount, :origin
end
