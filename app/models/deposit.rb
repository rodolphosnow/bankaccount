class Deposit < Event
    validates_presence_of :destination, :amount
end
