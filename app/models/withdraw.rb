class Withdraw < Event
    validates_presence_of :origin, :amount
end
