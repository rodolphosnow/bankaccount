class Event < ApplicationRecord
    validates_presence_of :type
    belongs_to :account, foreign_key: "origin", optional: true
    belongs_to :account, foreign_key: "destination", optional: true
end
