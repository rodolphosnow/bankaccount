class AccountsController < ApplicationController

    #method that will return de account balance
    def index
        account_id = params[:account_id]
        balance = AccountServices.get_account_balance(account_id)
        if balance.present?
            render json: balance, status: 200
        else
            render json: 0, status: 404
        end
    end
end
