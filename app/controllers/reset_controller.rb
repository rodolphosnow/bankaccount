class ResetController < ApplicationController
    
    #Reset delete all registers from the database
    def create
        Event.delete_all
        Account.delete_all
        render json: "OK", status: 200
    end
end
