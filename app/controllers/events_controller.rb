class EventsController < ApplicationController

    def create
        response = EventsServices.process_requested_event(event_params)
        status_code = 201
        if response == nil
            render json: 0, status: 404
        else
            render json: response, status: status_code
        end
        
    end



    private

    def event_params
        params.permit(:type, :destination, :amount, :origin)
    end
end
