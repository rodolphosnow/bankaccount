Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  resources :reset, path: 'reset', only: [:create]

  resources :accounts, path: 'balance', only: [:index]
  
  resources :events, path: "event", only: [:create]

end
