require 'rails_helper'

RSpec.describe Event, type: :model do
  describe "Associations" do
    it { should belong_to(:account).without_validating_presence }
  end
  
  it "is valid with valid attributes" do
    event = Event.new(type: "Deposit", amount: 10, destination: 1)
    expect(event).to be_valid
  end

  it "is not valid without a type" do
    event = Event.new(type: nil)
    expect(event).to_not be_valid
  end
end
