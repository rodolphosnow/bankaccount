require 'rails_helper'

RSpec.describe Account, type: :model do
  it "is valid with valid attributes" do
    account = Account.new(balance: 109)
    expect(account).to be_valid
  end

  it "is not valid without a balance" do
    account = Account.new(balance: nil)
    expect(account).to_not be_valid
  end

end
