require 'rails_helper'

RSpec.describe Deposit, type: :model do
  it "is valid with valid attributes" do
    deposit_event = Event.new(type: "Deposit", amount: 10, destination: 1)
    expect(deposit_event).to be_valid
  end

  it "is not valid without a destination" do
    deposit_event = Event.new(type: "Deposit", amount: 10, destination: nil)
    expect(deposit_event).to_not be_valid
  end

  it "is not valid without a amount" do
    deposit_event = Event.new(type: "Deposit", amount: nil, destination: 1)
    expect(deposit_event).to_not be_valid
  end
end
