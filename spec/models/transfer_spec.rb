require 'rails_helper'

RSpec.describe Transfer, type: :model do
  it "is valid with valid attributes" do
    transfer_event = Event.new(type: "Transfer", amount: 10, destination: 2, origin: 1)
    expect(transfer_event).to be_valid
  end
  
  it "is not valid without a origin" do
    transfer_event = Event.new(type: "Transfer", amount: 10, destination: 2, origin: nil)
    expect(transfer_event).to_not be_valid
  end

  it "is not valid without a destination" do
    transfer_event = Event.new(type: "Transfer", amount: 10, destination: nil, origin: 1)
    expect(transfer_event).to_not be_valid
  end

  it "is not valid without a amount" do
    transfer_event = Event.new(type: "Transfer", amount: nil, destination: 1, origin: 1)
    expect(transfer_event).to_not be_valid
  end
end
