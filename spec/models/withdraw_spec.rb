require 'rails_helper'

RSpec.describe Withdraw, type: :model do
  it "is valid with valid attributes" do
    withdraw_event = Event.new(type: "Withdraw", amount: 10, destination: 2, origin: 1)
    expect(withdraw_event).to be_valid
  end
  
  it "is not valid without a origin" do
    withdraw_event = Event.new(type: "Withdraw", amount: 10, destination: 2, origin: nil)
    expect(withdraw_event).to_not be_valid
  end

  it "is not valid without a amount" do
    withdraw_event = Event.new(type: "Withdraw", amount: nil, destination: 1, origin: 1)
    expect(withdraw_event).to_not be_valid
  end
end
