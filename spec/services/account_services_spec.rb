require 'spec_helper'
require "rails_helper"

RSpec.describe AccountServices do
    describe "AccountServices#get_account" do
        it "return nil if the requested account does not exist" do
            account = AccountServices.get_account(1234)
            expect(account).to eq(nil)
        end

        it "return the requested account if the account exist" do
            account = Account.create(balance: 100)
            account = AccountServices.get_account(1)
            expect(account).to eq(account)
        end
    end

    describe "AccountServices#get_account_balance" do
        it "return nil fi the request account does not exist" do
            balance = AccountServices.get_account_balance(1234)
            expect(balance).to eq(nil)
        end

        it "return the requested account balance if the account exist" do
            account = Account.create(balance: 100)
            balance = AccountServices.get_account_balance(account.id)
            expect(balance).to eq(account.balance)
        end
    end
end