require 'spec_helper'
require "rails_helper"

RSpec.describe EventsServices do
    describe "EventsServices#get_account" do
        it "return nil if the requested account does not exist" do
            account = EventsServices.get_account(1234)
            expect(account).to eq(nil)
        end

        it "return the requested account if the account exist" do
            account = Account.create(balance: 100)
            account = EventsServices.get_account(1)
            expect(account).to eq(account)
        end
    end

    describe "validate deposit" do
        it "when deposit data does not have amount" do
            deposit_data = {type: "deposit", amount: nil, destination: 1, amount: nil}
            service_return = EventsServices.deposit(deposit_data)
            expect(service_return).to eq(nil)
        end

        it "when deposit data does not have destination" do
            deposit_data = {type: "deposit", destination: nil, amount: 100}
            service_return = EventsServices.deposit(deposit_data)
            expect(service_return).to eq(nil)
        end

        it "when deposit data is valid" do
            deposit_data = {type: "deposit", amount: 100, destination: 1}
            service_return = EventsServices.deposit(deposit_data)
            expect(service_return).to eq({ destination: {id: deposit_data[:destination].to_s, balance: deposit_data[:amount]} })
        end

        it "when depositing in an existing account with valid data" do
            inicial_balance = 10
            account = Account.create(id: 4555, balance: inicial_balance)
            deposit_data = {type: "deposit", destination: 4555, amount: 10}
            total_balance = deposit_data[:amount] + inicial_balance
            service_return = EventsServices.deposit(deposit_data)
            expect(service_return).to eq({ destination: {id: deposit_data[:destination].to_s, balance: total_balance.to_f} })
        end
    end

    describe "Withdraw" do
        it "Withdraw from non-existing account" do
            withdraw_data = {type: "withdraw", origin: "200", amount: 10}
            service_return = EventsServices.withdraw(withdraw_data)
            expect(service_return).to eq(nil)
        end

        it "Withdraw from existent account" do
            inicial_balance = 100
            withdraw_amount = 5
            account = Account.create(id: 4555, balance: inicial_balance)
            withdraw_data = {type: "withdraw", amount: withdraw_amount, origin: account.id}
            total_balance = inicial_balance - withdraw_amount
            service_return = EventsServices.withdraw(withdraw_data)
            expect(service_return).to eq({ origin: {id: account.id.to_s, balance: total_balance.to_f} })
        end
    end

    describe "Transfer" do
        it "Transfer from non-existing account" do
            transfer_data = { type: "transfer", origin: "200", amount:15, destination: "300"}
            service_return = EventsServices.transfer(transfer_data)
            expect(service_return).to eq(nil)
        end

        it "Transfer from valid account" do
            inicial_origin_balance = 100
            inicial_destination_balance = 10
            transfer_amount = 5
            origin_account = Account.create(id: 4555, balance: inicial_origin_balance)
            destination_account = Account.create(id: 4552, balance: inicial_destination_balance)
            transfer_data = {type: "transfer", amount: transfer_amount, origin: origin_account.id, destination: destination_account.id}
            total_origin_balance = inicial_origin_balance - transfer_amount
            total_destination_balance = inicial_destination_balance + transfer_amount
            service_return = EventsServices.transfer(transfer_data)
            origin_account.reload
            destination_account.reload
            expect(service_return[:destination][:balance].to_f).to eq(total_destination_balance.to_f)
            expect(service_return[:origin][:balance].to_f).to eq(total_origin_balance.to_f)
            expect(service_return).to eq( { origin: {id: origin_account.id.to_s, balance: origin_account.balance.to_f}, destination: {id: destination_account.id.to_s, balance: destination_account.balance.to_f } })
        end
    end

end