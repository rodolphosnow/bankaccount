require 'rails_helper'

RSpec.describe "Resets", type: :request do
  describe "GET /reset" do

    it "return status and messaged when called" do
      post "/reset"
      expect(response.content_type).to eq("application/json; charset=utf-8")      
      expect(response).to have_http_status(:ok)
    end

  end
end
