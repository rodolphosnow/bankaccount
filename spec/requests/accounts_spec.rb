require 'rails_helper'

RSpec.describe "Accounts", type: :request do
  describe "GET /balance" do
    
    it "return status 404 when account is not found and 0 as balance" do
      get "/balance?account_id=1234"
      expect(response.content_type).to eq("application/json; charset=utf-8")
      expect(response).to have_http_status(404)
      expect(response.body).to eql("0")
    end

    it "return status 202 and balance when account found" do
      account = Account.create(balance: 100)
      get "/balance?account_id=#{account.id}"
      expect(response.content_type).to eq("application/json; charset=utf-8")
      expect(response).to have_http_status(200)
      parsed_response = JSON.parse(response.body)
      expect(parsed_response).to eql(account.balance.to_f)
    end
  
  end
end
