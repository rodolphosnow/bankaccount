class CreateEvents < ActiveRecord::Migration[6.0]
  def change
    create_table :events do |t|
      t.string :type
      t.integer :destination
      t.decimal :amount
      t.integer :origin

      t.timestamps
    end
  end
end
